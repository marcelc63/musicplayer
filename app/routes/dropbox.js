var express = require("express");
var router = express.Router();

var fetch = require("isomorphic-fetch"); // or another library of choice.
var Dropbox = require("dropbox").Dropbox;
var dbx = new Dropbox({
  accessToken:
    "r4yBWKENhkoAAAAAAAAJYYUMSpDrXOnVI91WC3CDx1RR0PUMfZGpXxVspY4fMXOx",
  fetch: fetch
});

/* GET ALL */
router.post("/", function(req, res) {
  let path = req.body.path;
  dbx
    .filesListFolder({ path, include_media_info: true })
    .then(function(response) {
      // console.log(response);
      let results = { success: true, content: response };
      res.header("Content-Type", "application/json");
      res.send(JSON.stringify(results, null, 4));
    })
    .catch(function(error) {
      console.log(error);
    });
});

router.post("/file", function(req, res) {
  let path = req.body.path;
  dbx
    .filesGetTemporaryLink({ path })
    .then(function(response) {
      console.log(response);
      let results = { success: true, content: response };
      res.header("Content-Type", "application/json");
      res.send(JSON.stringify(results, null, 4));
    })
    .catch(function(error) {
      console.log(error);
    });
});

module.exports = router;
