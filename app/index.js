//Dependencies
const express = require("express");
const app = express();
let http = require("http").Server(app);
let port = 3006;
var path = require("path");
var bodyParser = require("body-parser");

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  res.header("Access-Control-Allow-Methods", "*");
  next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var dropbox = require("./routes/dropbox");

app.use("/api/dropbox", dropbox);

app.get("/", function(req, res) {
  res.send("hi");
});

//Initiate
http.listen(port, function() {
  console.log("listening on *:", port);
});
