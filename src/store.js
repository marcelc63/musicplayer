import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import _ from "lodash";

export default new Vuex.Store({
  state: {
    folders: [],
    history: [{ path: "", name: "Home" }],
    file: {
      load: false,
      content: {}
    }
  },
  mutations: {
    change(state, payload) {
      let { key, data } = payload;
      state[key] = data;
    },
    history(state, payload) {
      let { type, data } = payload;
      if (type === "HISTORY_BACK") {        
        state.history = state.history.slice(0, data + 1);
      } else if (type === "HISTORY_ADD") {
        state.history = state.history.concat({
          path: data.path_lower,
          name: data.name
        });
        state.history = _.uniqBy(state.history, "name");
      }
    },
    folders(state, payload) {
      let { type, data } = payload;
      if (type === "FOLDERS_ADD") {
        state.folders = data.filter(x => {
          if (x[".tag"] === "folder") {
            return true;
          }
          if (x[".tag"] === "file") {
            let check = x.name.split(".");
            let format = check[check.length - 1];
            if (format === "mp3") {
              return true;
            }
          }
        });
      }
    },
    file(state, payload) {
      let { key, data } = payload;
      state.file[key] = data;
    }
  },
  actions: {
    change({ commit }, payload) {
      commit("change", payload);
    },
    history({ commit }, payload) {
      commit("history", payload);
    },
    file({ commit }, payload) {
      commit("file", payload);
    },
    folders({ commit }, payload) {
      commit("folders", payload);
    }
  },
  getters: {
    state(state) {
      return state;
    },
    history(state) {
      return state.history;
    },
    folders(state) {
      return state.folders;
    },
    file(state) {
      return state.file;
    }
  }
});
